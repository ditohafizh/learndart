void main() {
  Function minus = (int a, int b) => print(a - b);

  someOtherFunction("Hello", minus);
  Function returnFunc = otherFunction();
  print(returnFunc(4, 2));
}

void someOtherFunction(String message, Function fromFunction) {
  print(message);
  fromFunction(4, 2);
}

Function otherFunction() {
  return (int a, int b) => a * b;
}