void main() {
  var student1 = new Student(32, "John");
  print("${student1.id} & ${student1.name}");
  student1.study();
  student1.sleep();
  print("");

  var student2 = new Student(22, "Marie");
  print("${student2.id} & ${student2.name}");
  student2.study();
  student2.sleep();
  print("");

  var student3 = new Student.customConstructor();
  student3.id = 99;
  student3.name = "Custo";
  print("${student3.id} & ${student3.name}");
  student3.study();
  student3.sleep();
  print("");

  var student4 = new Student.customNamedConstructor(name: "Shinta", id: 12);
  print("${student4.id} & ${student4.name}");
  student4.study();
  student4.sleep();
  print("");

  var student5 = new CustomStudent();
  student5.percentage = 483;
  print(student5.percentage);
}

class Student {
  int id = -1;
  String name;

  Student(this.id, this.name);

  Student.customConstructor() {}

  Student.customNamedConstructor({ id: int, name: String }) {
    this.id = id;
    this.name = name;
  }

  void study() {
    print("${this.name} is studying");
  }

  void sleep() {
    print("${this.name} is sleeping");
  }
}

class CustomStudent {

  double _percent;

  void set percentage(double markScore) => _percent = (markScore / 500) * 100;

  double get percentage => _percent;
}