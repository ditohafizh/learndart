void main() {
  // Number

  int score = 11;
  var count = 23;

  int hexCode = 0xAEFDEEED;

  double flight = 42.2;
  var fly = 25.6;

  double rocket = 14e5;

  // Strings
  String come = "I'm coming";
  var leave = "I leave";

  // Boolean
  bool isValid;
  var isNotValid = false;

  print(rocket);
  print(isValid);
}