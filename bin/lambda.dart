void main() {
  Function addTwoNumbers = (int a, int b) => print(a + b);
  
  var multiplyByFour = (int a) => a * 4;
  
  addTwoNumbers(4, 2);
  print(multiplyByFour(2));
}