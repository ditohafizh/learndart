/*
Example of line break
Comment
 */

void main() {
  // hello world
  print("Hello World!");

  // second comment
  print("This is my first Dart application");

  print(4 * 3);
  
  print(false);
}