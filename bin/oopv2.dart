void main() {
  var dog = new Dog("John", "red");
  dog.breed = "breedv1";
  dog.eat();

  var cat = new Cat("Chena", "blue");
  cat.daugh = "daughv1";
  cat.meow();

  var animal = new Animal("green");
  animal.eat();
}

class Animal {
  String color;

  Animal(this.color) {
    print("Color of Animal is ${color}");
  }

  void eat() {
    print("Animal is Eating!\n");
  }
}

class Dog extends Animal {
  String breed;

  String color = "blue";

  Dog(String name, this.color): super(color) {}

  void eat() {
    print("Dog is Eating!\n");
  }

  void bark() {
    print("Bark!\n");
  }
}

class Cat extends Animal {
  String daugh;

  Cat(String name, String color): super(color) {}

  void meow() {
    print("Meow!\n");
  }
}