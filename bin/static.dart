void main() {
  print("Pi of Circle is ${Circle.pi}");
}

class Circle {
  static const pi = 3.14;
  static int maxLength = 5;

  static void calculateArea() {
    print("Do calculation");
  }
}