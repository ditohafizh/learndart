void main() {
  countPerimeter(4, 2);
  print("The area is ${countArea(4, 2)}");

  printCities("US", "Indonesia", "Bali");
  printCitiesOptional("US");

  findVolume(3, width: 5, height: 3);
}

void countPerimeter(int length, int breadth) {
  int perimeter = 2 * (length + breadth);
  print("The perimeter is $perimeter");
}

int countArea(int length, int breadth) => length * breadth;

// required parameters
void printCities(String city1, String city2, String city3) {
  print("Name of City1 $city1");
  print("Name of City2 $city2");
  print("Name of City3 $city3");
}

// optional parameters
void printCitiesOptional(String city1, [String city2, String city3]) {
  print("Name of City1 $city1");
  print("Name of City2 $city2");
  print("Name of City3 $city3");
}

// named parameters
void findVolume(int a, { height: int, width: int }) {
  print("The volume is ${a * height * width}");
}